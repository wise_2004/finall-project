document.addEventListener('DOMContentLoaded', function () {
    const registrationForm = document.getElementById('registrationForm');
    const postForm = document.getElementById('postForm');
    const postList = document.getElementById('postList');

    registrationForm.addEventListener('submit', handleRegistrationSubmit);
    postForm.addEventListener('submit', handlePostSubmit);

    function handleRegistrationSubmit(event) {
        event.preventDefault();
        // Handle user registration logic
        // For example, you can call a function like registerUser() here
    }

    function handlePostSubmit(event) {
        event.preventDefault();
        // Handle post creation logic
        createPost();
    }

    function createPost() {
        const postDate = document.getElementById('postDate').value;
        const postCategory = document.getElementById('postCategory').value;
        const postText = document.getElementById('postText').value;
        const postImage = document.getElementById('postImage').value;

        const postElement = document.createElement('div');
        postElement.innerHTML = `
            <div>
                <h3>${postDate} - ${postCategory}</h3>
                <p>${postText}</p>
                <img src="${postImage}" alt="Post Image">
            </div>
        `;

        postList.appendChild(postElement);
    }
});
