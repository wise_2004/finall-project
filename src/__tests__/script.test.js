const { JSDOM } = require('jsdom');
const fs = require('fs');
const path = require('path');

// Load HTML content
const htmlContent = fs.readFileSync(path.resolve(__dirname, '../index.html'), 'utf-8');
const dom = new JSDOM(htmlContent);
global.document = dom.window.document;

// Load JavaScript content
const scriptContent = fs.readFileSync(path.resolve(__dirname, '../script.js'), 'utf-8');
dom.window.eval(scriptContent);

// Import the `expect` function correctly
const { expect } = require('@jest/globals');

describe('Script Tests', () => {
  // Helper function to wait for asynchronous tasks to complete
  const waitForAsyncTasks = () => new Promise((resolve) => setImmediate(resolve));

  it('should create a post with the correct content', async () => {
    // Set up form elements with test data
    document.getElementById('postDate').value = '2022-01-05';
    document.getElementById('postCategory').value = 'Future';
    document.getElementById('postText').value = 'This is a test post.';
    document.getElementById('postImage').value = 'test-image.jpg';

    // Trigger form submission event
    const postForm = document.getElementById('postForm');
    postForm.dispatchEvent(new dom.window.Event('submit'));

    // Wait for asynchronous tasks to complete
    await waitForAsyncTasks();

    // Verify that the postList contains the created post with the correct content
    const postList = document.getElementById('postList');
    const createdPost = postList.querySelector('div');

    // Assertions using Jest's `expect` function
    expect(createdPost).toBeDefined();

    // Use a more flexible regular expression
    expect(createdPost.innerHTML).toMatch(/2022-01-05 - [\s\S]*Future/);
    expect(createdPost.innerHTML).toContain('This is a test post.');
    expect(createdPost.innerHTML).toContain('src="test-image.jpg"');
  });
});
